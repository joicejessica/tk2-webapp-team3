const movieContainer = document.getElementById("moviesContainer");
const movieGenreSelect = document.getElementById("movieGenreSelect");
const sortSelect = document.getElementById("sortSelect");
const keywordInput = document.getElementById("keywordInput");
const findButton = document.getElementById("findButton");
const loadingTrailer = document.getElementById("loadingTrailer");
const previewModalElement = document.getElementById("modal");
const previewModal = new bootstrap.Modal(previewModalElement, {});

const defaultGenre = "All";

const loadGenres = () => {
  const genres = ["All", ...getGenres()];

  genres.forEach((genre) => {
    const option = document.createElement("option");
    option.innerHTML = genre;
    option.value = genre;

    movieGenreSelect.appendChild(option);
  });
};

const loadMovies = () => {
  const genre = movieGenreSelect.value;
  const sort = sortSelect.value;
  const keyword = keywordInput.value;

  let movies = getMovies();

  if (keyword) {
    movies = movies.filter((f) => f.title.toLowerCase().includes(keyword.toLowerCase()));
  }

  if (genre !== "All") {
    movies = movies.filter((f) => f.genres.includes(genre));
  }

  if (sort === "asc") {
    movies = movies.sort((a, b) => (a.title > b.title ? 1 : -1));
  } else {
    movies = movies.sort((a, b) => (a.title < b.title ? 1 : -1));
  }

  moviesContainer.innerHTML = "";

  movies.forEach((item) => {
    const { title, poster, overview, genres, release_date, rating, trailer } = item;
    const year = new Date(release_date).getFullYear();

    const movieContent = `<div class="card h-100">
      <div class="poster">
        <div class="play-bg"><div class="play-button" onClick="onPreviewClick('${trailer}')"><i class="fas fa-play"></i></div></div>
        <img src="assets/image/${poster}" class="card-img-top" alt="..." />
      </div>
      <div class="card-body">
        <h5 class="card-title mb-0">${title}</h5>
        <div class="rating">${rating}</div>
        <div class="mb-3"><small>${year}</small></div>
        <p class="card-description">${overview}</p>
        <div>
            ${genres.map((genre) => `<span class="badge rounded-pill bg-primary me-2">${genre}</span>`).join("")}
        </div>
      </div>
    </div>`;

    const cardItem = document.createElement("div");
    cardItem.className = "col";
    cardItem.innerHTML = movieContent;

    moviesContainer.appendChild(cardItem);
  });
};

window.addEventListener("DOMContentLoaded", (event) => {
  loadGenres();
  loadMovies();
});

movieGenreSelect.addEventListener("change", (event) => {
  loadMovies();
});

sortSelect.addEventListener("change", (event) => {
  loadMovies();
});

findButton.addEventListener("click", (event) => {
  loadMovies();
});

const onPreviewClick = (trailer) => {
  previewContainer.src = trailer;
  previewModal.show();
};

previewModalElement.addEventListener("hidden.bs.modal", function (event) {
  previewContainer.src = "";
});
