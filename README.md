# tk2-webapp-team3

## Team 3 Members :
1) 2301961992 - Joice Jessica
2) 2301965763 - Muhammad Abidzar Arsyidin
3) 2301962055 - Mochamad Irfan Firdaus
4) 2301964281 - Lidia Octaviana Putri

## Description
Gallery Film yang terdiri dari feature:
a. Terdapat gallery untuk menampilkan seluruh film
b. Terdapat kategori untuk film yang ditampilkan
c. Setiap film yang ditampilkan dapat diputar di dalam web
d. Terdapat fitur sorting dari A-Z untuk film yang ditampilkan 
e. Terdapat fitur search untuk pencarian film.

